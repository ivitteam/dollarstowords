﻿using DollarsToWordsWpfClient.DollarsToWordsService;
using MahApps.Metro.Controls.Dialogs;

namespace DollarsToWordsWpfClient
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class DollarsToWordsConverter
	{
		public DollarsToWordsConverter()
		{
			InitializeComponent();
		}

		private async void ButtonConvert_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			using (var service  = new ConvertDollarsToWordsClient())
			{
				var result = service.Convert(TextBoxInputAmount.Text);
				await this.ShowMessageAsync("Result", result);
				service.Close();
			}
			
		}
	}
}
