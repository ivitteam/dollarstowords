# DollarsToWords

1. Solutions
	- Library
	- Wcf - http://wcf.ivit.org.pl/ConvertDollarsToWords.svc
	- Wpf
	- Api
	- Web

2. Projects

	a. Library
		- DollarsToWordsLogic - .NET Framework 4.6.2 class library
		- XUnitTestDollarsToWordsLogic - xUnit tests for DollarsToWordsLogic class library
		
	b. Wcf
		- DollarsToWordsService - .NET Framework 4.7.2 WcfService
		- URL - http://wcf.ivit.org.pl/ConvertDollarsToWords.svc

	c. Wpf
		- DollarsToWordsWpfClient - WPF C# Client App
		- UI - MahApps.Metro
		
	d. Api
		- DollarsToWordsApi - .NET Core 2.2 REST API
		- URL - https://webapi.ivit.org.pl/swagger/index.html
		
	e. Web
		- DollarsToWordsWeb - .NET Core 2.2 Razor Pages client application
		- URL - https://dollars2words.ivit.org.pl
		


