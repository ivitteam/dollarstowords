﻿using System;
using DollarsToWordsLogic.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DollarsToWordsApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class DollarsToWordsController : ControllerBase
	{
		private readonly IDollarsConverter _dollarsConverter;

		/// <inheritdoc />
		public DollarsToWordsController(IDollarsConverter dollarsConverter)
		{
			_dollarsConverter = dollarsConverter;
		}

		[HttpGet]
		public ActionResult<string> Convert(string amount)
		{
			try
			{
				return Ok(_dollarsConverter.Convert(amount));
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}