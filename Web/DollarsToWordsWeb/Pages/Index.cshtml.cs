﻿using System.Threading.Tasks;
using DollarsToWordsService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DollarsToWordsWeb.Pages
{
	public class IndexModel : PageModel
	{
		public async Task<JsonResult> OnGetWcfResult(string amount)
		{
			var service = new ConvertDollarsToWordsClient();
			var result = await service.ConvertAsync(amount);
			await service.CloseAsync();
			return new JsonResult(result);
		}
	}
}
