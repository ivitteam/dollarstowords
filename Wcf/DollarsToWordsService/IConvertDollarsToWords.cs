﻿using System.ServiceModel;

namespace DollarsToWordsService
{
	[ServiceContract]
	public interface IConvertDollarsToWords
	{
		[OperationContract]
		string Convert(string amount);
	}
}
