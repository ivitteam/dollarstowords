﻿using System;
using DollarsToWordsLogic.Infrastructure.Interfaces;
using DollarsToWordsLogic.Infrastructure.Services;

namespace DollarsToWordsService
{
	public class ConvertDollarsToWords : IConvertDollarsToWords
	{
		public string Convert(string amount)
		{
			try
			{
				IDollarsConverter converter = new DollarsConverter();
				return converter.Convert(amount);
			}
			catch (Exception e)
			{
				return $"Something is not yes... {e.Message}";
			}
		}
	}
}