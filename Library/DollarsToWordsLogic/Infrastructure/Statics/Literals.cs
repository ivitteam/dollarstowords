﻿namespace DollarsToWordsLogic.Infrastructure.Statics
{
	public static class Literals
	{
		public static readonly string[] UnderTwenty =
		{
			"zero",
			"one",
			"two",
			"three",
			"four",
			"five",
			"six",
			"seven",
			"eight",
			"nine",
			"ten",
			"eleven",
			"twelve",
			"thirteen",
			"fourteen",
			"fifteen",
			"sixteen",
			"seventeen",
			"eighteen",
			"nineteen"
		};

		public static readonly string[] TwentyOrMore =
		{
			"ten",
			"twenty",
			"thirty",
			"forty",
			"fifty",
			"sixty",
			"seventy",
			"eighty",
			"ninety"
		};

		public static readonly string[] BigNumbers =
		{
			"hundred",
			"thousand",
			"million"
		};

		public static readonly string[] Currency =
		{
			"cent",
			"cents",
			"dollar",
			"dollars"
		};
	}
}