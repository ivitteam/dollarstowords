﻿using System.Text.RegularExpressions;

namespace DollarsToWordsLogic.Infrastructure.Statics
{
	public static class AmountValidator
	{
		public static bool Validate(string amount)
		{
			if (!amount.Contains(",")) return ValidateDollars(amount);

			var splittedAmount = amount.Split(',');
			if (splittedAmount.Length != 2) return false;
			return ValidateCents(splittedAmount[1]) && ValidateDollars(splittedAmount[0]);
		}

		private static bool ValidateCents(string amount) => Regex.IsMatch(amount, Regexes.Cents);

		private static bool ValidateDollars(string amount) =>
			Regex.IsMatch(amount, Regexes.Dollars0To999) ||
			Regex.IsMatch(amount, Regexes.Dollars1000To999999) ||
			Regex.IsMatch(amount, Regexes.Dollars1000000To999999999);
	}
}