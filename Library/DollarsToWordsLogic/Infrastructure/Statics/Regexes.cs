﻿namespace DollarsToWordsLogic.Infrastructure.Statics
{
	public static class Regexes
	{
		public const string Cents = @"^(\d?[1-9]|[1-9]0)$";
		public const string Dollars0To999 = @"^(?:0|[1-9]\d{0,2})$";
		public const string Dollars1000To999999 = @"^([1-9]\d{0,2}) [0-9]{3}$";
		public const string Dollars1000000To999999999 = @"^([1-9]\d{0,2}) [0-9]{3} [0-9]{3}$";
	}
}
