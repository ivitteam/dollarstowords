﻿using System;
using System.Linq;
using DollarsToWordsLogic.Infrastructure.Interfaces;
using DollarsToWordsLogic.Infrastructure.Statics;

namespace DollarsToWordsLogic.Infrastructure.Services
{
	public class DollarsConverter : IDollarsConverter
	{
		public string Convert(string amount)
		{
			if (!AmountValidator.Validate(amount))
				throw new ArgumentOutOfRangeException(nameof(amount), "Wrong amount, pleasy insert valid data.");

			return amount.Contains(',')
				? $"{ConvertDollars(amount.Split(',')[0])} and {ConvertCents(amount.Split(',')[1])}"
				: $"{ConvertDollars(amount)}";
		}

		private static string ConvertDollars(string amount)
		{
			if (CheckIfZero(amount)) return $"{Literals.UnderTwenty[0]} {Literals.Currency[3]}";
			if (CheckIfOne(amount)) return $"{Literals.UnderTwenty[1]} {Literals.Currency[2]}";

			var splittedNumbers = amount.Split(' ');
			if (splittedNumbers.Any(p => p.Length > 3)) return string.Empty;

			var wordNumbers = splittedNumbers.Select(ConvertNumber).ToList();

			var result = string.Empty;
			var counter = wordNumbers.Count - 1;

			foreach (var word in wordNumbers)
			{
				if (counter > 0)
				{
					result += $"{word} {Literals.BigNumbers[counter]} ";
				}
				else
				{
					result += $"{word} ";
				}

				counter--;
			}

			return result + Literals.Currency[3];
		}

		private static string ConvertCents(string amount)
		{
			if (amount.Length == 1) return $"{Literals.TwentyOrMore[int.Parse(amount) - 1]} {Literals.Currency[1]}";
			return CheckIfOne(amount)
				? $"{Literals.UnderTwenty[1]} {Literals.Currency[0]}"
				: $"{ConvertTrailingTwoDigits(amount)} {Literals.Currency[1]}";
		}

		private static string ConvertNumber(string amount)
		{
			if (!int.TryParse(amount, out var number) || number < 1 || number > 999) return string.Empty;

			var hundreds = ConvertHundreds(amount);
			var underHundreds = number > 99 ? ConvertTrailingTwoDigits(amount.Substring(1,2)) : ConvertTrailingTwoDigits(amount);

			return string.IsNullOrEmpty(hundreds) && string.IsNullOrEmpty(underHundreds)
				? string.Empty
				: string.IsNullOrEmpty(hundreds)
					? underHundreds
					: string.IsNullOrEmpty(underHundreds)
						? hundreds
						: $"{hundreds} {underHundreds}";
		}

		private static string ConvertHundreds(string amount)
		{
			if (!int.TryParse(amount, out var number) || number < 100 || number >= 1000)
				return string.Empty;
			return $"{Literals.UnderTwenty[number / 100]} {Literals.BigNumbers[0]}";
		}

		private static string ConvertTrailingTwoDigits(string amount)
		{
			var result = string.Empty;
			if (!int.TryParse(amount, out var twoDigitNumber) || twoDigitNumber < 0 || twoDigitNumber >= 100)
				return result;

			var tens = twoDigitNumber / 10 - 1;
			var units = twoDigitNumber % 10;

			if (twoDigitNumber >= 20)
			{
				result += Literals.TwentyOrMore[tens];
				if (units != 0)
				{
					result += $"-{Literals.UnderTwenty[units]}";
				}
			}
			else if (twoDigitNumber != 0)
			{
				result += Literals.UnderTwenty[twoDigitNumber];
			}

			return result;
		}

		private static bool CheckIfOne(string amount) => int.TryParse(amount, out var number) && number == 1;
		private static bool CheckIfZero(string amount) => int.TryParse(amount, out var number) && number == 0;
	}
}