﻿namespace DollarsToWordsLogic.Infrastructure.Interfaces
{
	public interface IDollarsConverter
	{
		string Convert(string amount);
	}
}