using DollarsToWordsLogic.Infrastructure.Statics;
using Xunit;

namespace XUnitTestDollarsToWordsLogic
{
	public class AmountValidatorTest
	{
		[Theory]
		[InlineData("999 999 999,99", true)]
		[InlineData("45 100", true)]
		[InlineData("0,01", true)]
		[InlineData("25,1", true)]
		[InlineData("1", true)]
		[InlineData("0", true)]
		[InlineData(",99", false)]
		[InlineData("123 999 999 999,99", false)]
		[InlineData("999 99,99", false)]
		[InlineData("001,99", false)]
		[InlineData("999,", false)]
		[InlineData("9,9912351235235", false)]
		[InlineData("001 999 999,99", false)]
		[InlineData("001 999,99", false)]
		[InlineData("0,0", false)]
		[InlineData("0,00", false)]
		public void Validate(string entryData, bool expected)
		{
			// Act  
			var validationResult = AmountValidator.Validate(entryData);
  
			//Assert  
			Assert.Equal(expected, validationResult);
		}
	}
}