using System;
using System.Reflection;
using DollarsToWordsLogic.Infrastructure.Interfaces;
using DollarsToWordsLogic.Infrastructure.Services;
using Xunit;

namespace XUnitTestDollarsToWordsLogic
{
	public class DollarsConverterTest
	{
		[Theory]
		[InlineData("0", "")]
		[InlineData("1", "one")]
		[InlineData("2", "two")]
		[InlineData("10", "ten")]
		[InlineData("99", "ninety-nine")]
		[InlineData("54", "fifty-four")]
		[InlineData("19", "nineteen")]
		public void ConvertTrailingTwoDigits(string entryData, string expected)
		{
			var dollarsConverter = new DollarsConverter();

			var convertTrailingTwoDigitsMethod = dollarsConverter.GetType().GetMethod("ConvertTrailingTwoDigits",
				BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);

			if (convertTrailingTwoDigitsMethod == null)
			{
				Assert.True(false, "Cant find ConvertTrailingTwoDigits method");
			}

			var result = (string) convertTrailingTwoDigitsMethod.Invoke(typeof(string), new object[] {entryData});

			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("100", "one hundred")]
		[InlineData("912", "nine hundred")]
		[InlineData("1", "")]
		[InlineData("2324", "")]
		public void ConvertHundreds(string entryData, string expected)
		{
			var dollarsConverter = new DollarsConverter();

			var convertHundreds = dollarsConverter.GetType().GetMethod("ConvertHundreds",
				BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);

			if (convertHundreds == null)
			{
				Assert.True(false, "Cant find ConvertHundreds method");
			}

			var result = (string) convertHundreds.Invoke(typeof(string), new object[] {entryData});

			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("1", "one")]
		[InlineData("5", "five")]
		[InlineData("12", "twelve")]
		[InlineData("23", "twenty-three")]
		[InlineData("45", "forty-five")]
		[InlineData("123", "one hundred twenty-three")]
		[InlineData("111", "one hundred eleven")]
		[InlineData("224", "two hundred twenty-four")]
		[InlineData("300", "three hundred")]
		[InlineData("999", "nine hundred ninety-nine")]
		[InlineData("003", "three")]
		[InlineData("023", "twenty-three")]
		[InlineData("04", "four")]
		[InlineData("0", "")]
		[InlineData("00", "")]
		[InlineData("000", "")]
		[InlineData("1546", "")]
		[InlineData("20005", "")]
		public void ConvertNumber(string entryData, string expected)
		{
			var dollarsConverter = new DollarsConverter();

			var convertNumber = dollarsConverter.GetType().GetMethod("ConvertNumber",
				BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);

			if (convertNumber == null)
			{
				Assert.True(false, "Cant find ConvertNumber method");
			}

			var result = (string) convertNumber.Invoke(typeof(string), new object[] {entryData});

			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("01", "one cent")]
		[InlineData("09", "nine cents")]
		[InlineData("1", "ten cents")]
		[InlineData("2", "twenty cents")]
		[InlineData("10", "ten cents")]
		[InlineData("99", "ninety-nine cents")]
		[InlineData("54", "fifty-four cents")]
		[InlineData("19", "nineteen cents")]
		public void ConvertCents(string entryData, string expected)
		{
			var dollarsConverter = new DollarsConverter();

			var convertCentsMethod = dollarsConverter.GetType().GetMethod("ConvertCents",
				BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);

			if (convertCentsMethod == null)
			{
				Assert.True(false, "Cant find ConvertCents method");
			}

			var result = (string) convertCentsMethod.Invoke(typeof(string), new object[] {entryData});

			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("999 999 999",
			"nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars")]
		[InlineData("0", "zero dollars")]
		[InlineData("1", "one dollar")]
		[InlineData("123 252", "one hundred twenty-three thousand two hundred fifty-two dollars")]
		[InlineData("12 111", "twelve thousand one hundred eleven dollars")]
		[InlineData("14145", "")]
		[InlineData("23", "twenty-three dollars")]
		public void ConvertDollars(string entryData, string expected)
		{
			var dollarsConverter = new DollarsConverter();

			var convertDollarsMethod = dollarsConverter.GetType().GetMethod("ConvertDollars",
				BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);

			if (convertDollarsMethod == null)
			{
				Assert.True(false, "Cant find ConvertDollars method");
			}

			var result = (string) convertDollarsMethod.Invoke(typeof(string), new object[] {entryData});

			Assert.Equal(expected, result);
		}

		[Theory]
		[InlineData("999 999 999,99",
			"nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents")]
		[InlineData("45 100", "forty-five thousand one hundred dollars")]
		[InlineData("0,01", "zero dollars and one cent")]
		[InlineData("25,1", "twenty-five dollars and ten cents")]
		[InlineData("1", "one dollar")]
		[InlineData("0", "zero dollars")]
		[InlineData("0,0", "")]
		[InlineData(",11110", "")]
		[InlineData("0,032", "")]
		[InlineData("12330", "")]
		[InlineData("999 999 999 999", "")]
		public void Convert(string entryData, string expected)
		{
			try
			{
				// Act  
				IDollarsConverter converter = new DollarsConverter();
				var convertionResult = converter.Convert(entryData);

				//Assert
				Assert.Equal(expected, convertionResult);
			}
			catch (Exception e)
			{
				//Assert
				Assert.Equal(expected, string.Empty);
			}
			

		}
	}
}